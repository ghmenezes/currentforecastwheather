import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ForecastService {

  constructor(
    private http: HttpClient
  ) { }

  getCityToday(city: string): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/weather?q=${city}&appid=${environment.apiKey}`);
  }
  
  getHourForecast(city: string): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/forecast?q=${city}&appid=${environment.apiKey}`);
  }

}
