import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForecastService } from './services/forecast.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    ForecastService,
  ],
})
export class CoreModule { }
