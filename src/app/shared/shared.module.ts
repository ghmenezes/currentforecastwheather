import { NgModule } from '@angular/core';
import { SearchComponent } from './components/search/search.component';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';
import { HeaderComponent } from './components/header/header.component';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TemperatureConverterPipe } from './pipes/temperature-converter.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CoreModule,
    MaterialModule
  ],
  declarations: [
    SearchComponent,
    HeaderComponent,
    TemperatureConverterPipe
  ],
  exports: [
    SearchComponent,
    HeaderComponent,
    TemperatureConverterPipe
  ]
})
export class SharedModule { }
