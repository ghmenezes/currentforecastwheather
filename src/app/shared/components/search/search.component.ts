import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ForecastService } from 'src/app/core/services/forecast.service';

declare var toastr: any;

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  form: FormGroup;
  city: string;

  constructor(
    private formBuilder: FormBuilder, 
    private router: Router,
    private forecastService: ForecastService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.form = this.formBuilder.group({
      city: [null, [Validators.required]]
    });
    this.activatedRoute.params.subscribe(params => {
      if(params['city']) {
        this.city = params['city'];
        this.form.controls['city'].setValue(params['city'].replace("-"," "));
      }
    });
  }

  onSubmit() {
    console.log(this.form.value);
    if (this.form.valid) {
      const $this = this;
      this.forecastService.getCityToday(this.form.value.city.toLowerCase().replace("-"," ")).subscribe(
        (response) => {
          $this.router.navigate([`/${this.form.value.city.toLowerCase().replace(" ","-")}`]);
        }, (error) => {
          toastr.info('Esta cidade não existe ou digitou errado');
        });
    } else {
      toastr.info('Campo cidade é obrigatório');
    }
  }

}
