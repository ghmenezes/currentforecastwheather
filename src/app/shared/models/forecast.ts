import { ForecastList } from './forecast-list';

export interface Forecast {    
    cod: string;
    message: any;
    cnt: number;
    list: ForecastList[];
}
