export interface ForecastList {
    dt: number;
    main: any;
    weather: any[];
    clouds: any;
    wind: any;
    sys: any;
    dt_txt: any;
}
