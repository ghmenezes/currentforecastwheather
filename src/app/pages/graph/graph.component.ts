import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ForecastService } from 'src/app/core/services/forecast.service';
import { Forecast } from 'src/app/shared/models/forecast';
import { ForecastList } from 'src/app/shared/models/forecast-list';

import * as moment from 'moment';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css']
})
export class GraphComponent implements OnInit {

  city: string;
  forecast: Forecast;
  ForecastList: ForecastList[] = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private forecastService: ForecastService,
    private router: Router
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.city = params['city'];
    });
    this.getHourForecast();
  }

  getHourForecast() {
    const $this = this;
    this.forecastService.getHourForecast(this.city.toLowerCase().replace("-"," ")).subscribe(
      (response) => {
        this.forecast = response;
        this.ForecastList = response.list;
      }, (error) => {
        alert('Algo errado aconteceu');
        $this.router.navigate([`/`]);
      });
  }
  
  isSame(item, itemForecast) {
    console.log(moment(this.dateTransformDefault(item)).isSame(this.dateTransformDefault(itemForecast), 'day'));
    if ( moment(this.dateTransformDefault(item)).isSame(this.dateTransformDefault(itemForecast), 'day') ) {
      return true;
    }
    return false;
  }

  dateTransformDefault(date) {
    return moment(date).format('YYYY-MM-DD');
  }

  dateTransform(date) {
    return moment(date).format('DD/MM/YYYY');
  }

}
