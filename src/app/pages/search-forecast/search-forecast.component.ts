import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ForecastService } from 'src/app/core/services/forecast.service';

declare var toastr: any;

@Component({
  selector: 'app-search-forecast',
  templateUrl: './search-forecast.component.html',
  styleUrls: ['./search-forecast.component.css']
})
export class SearchForecastComponent implements OnInit {

  city: string;
  forecast: any;
  weather: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private forecastService: ForecastService,
    private router: Router
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.city = params['city'];
    });
    this.getCityToday();
  }

  getCityToday() {
    this.forecastService.getCityToday(this.city.toLowerCase().replace("-"," ")).subscribe(
      (response) => {
        this.forecast = response;
        this.weather = response.weather[0].icon;
      }, (error) => {
        toastr.info('Ops! Algo errado esta acontecendo');
        this.router.navigate([`/`]);
      });
  }

  changeGraph() {
    this.router.navigate([`/${this.city.toLowerCase()}/graph`]);
  }

}
