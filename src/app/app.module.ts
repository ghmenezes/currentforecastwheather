import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { MaterialModule } from './material.module';

// Modulos compartilhados
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Rota da aplicação
import { AppRoutingModule } from './routing/app-routing.module';

// Paginas
import { HomepageComponent } from './pages/homepage/homepage.component';
import { SearchForecastComponent } from './pages/search-forecast/search-forecast.component';
import { GraphComponent } from './pages/graph/graph.component';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    SearchForecastComponent,
    GraphComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MaterialModule,
    CoreModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
