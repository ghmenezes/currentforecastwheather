import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from '../pages/homepage/homepage.component';
import { SearchForecastComponent } from '../pages/search-forecast/search-forecast.component';
import { GraphComponent } from '../pages/graph/graph.component';

const routes: Routes = [
  { path: '', component: HomepageComponent },
  { path: ':city', component: SearchForecastComponent },
  { path: ':city/graph', component: GraphComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
